## [1.1.4](https://gitlab.com/jeff_cook/semantic-release/compare/v1.1.3...v1.1.4) (2020-08-07)


### Bug Fixes

* remove package files from image ([42b22ad](https://gitlab.com/jeff_cook/semantic-release/commit/42b22ad07fdbef00bde84e0af80da5ea693c1c3a))

## [1.1.3](https://gitlab.com/jeff_cook/semantic-release/compare/v1.1.2...v1.1.3) (2020-08-06)


### Bug Fixes

* ignore artifacts ([a03b5bf](https://gitlab.com/jeff_cook/semantic-release/commit/a03b5bfb1d68570b5780c7fe3cb4bb68237f01a7))

## [1.1.2](https://gitlab.com/jeff_cook/semantic-release/compare/v1.1.1...v1.1.2) (2020-08-06)


### Bug Fixes

* run fix to update node packages ([606d3d0](https://gitlab.com/jeff_cook/semantic-release/commit/606d3d0a8d2861a1f484336b182647bfdbac7cf4))

## [1.1.1](https://gitlab.com/jeff_cook/semantic-release/compare/v1.1.0...v1.1.1) (2020-08-06)


### Bug Fixes

* update node and alpine ([5500cbc](https://gitlab.com/jeff_cook/semantic-release/commit/5500cbc01ed11550fce30f9ffb2317ca7c519908))

# [1.1.0](https://gitlab.com/jeff_cook/semantic-release/compare/v1.0.0...v1.1.0) (2020-08-06)


### Features

* use existing last stage ([703305e](https://gitlab.com/jeff_cook/semantic-release/commit/703305e00db83d7669caa7cc02b1b3d2bbdfa290))

# 1.0.0 (2020-07-30)


### Features

* first draft ([7c290fb](https://gitlab.com/jeff_cook/semantic-release/commit/7c290fb822338bd44eb0dda694be607be7c5de71))
